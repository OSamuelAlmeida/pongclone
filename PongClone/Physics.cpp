#include "Physics.h"

namespace Physics
{
	bool intersects(const std::shared_ptr<PhysicalGameObject>& object1, const std::shared_ptr<PhysicalGameObject>& object2)
	{
		return object1->getRect().intersects(object2->getRect());
	}

	bool collideWith(std::shared_ptr<PhysicalGameObject>& object, const std::vector<std::shared_ptr<PhysicalGameObject>>& others, std::shared_ptr<PhysicalGameObject>& otherObject)
	{
		for (auto& other : others)
		{
			if (other == object)
			{
				continue;
			}

			if (intersects(object, other))
			{
				otherObject = other;
				return true;
			}
		}

		return false;
	}
}