#pragma once

#include <memory>

#include "GameStateManager.h"

class PongStateManager : public GameStateManager
{
public:
	explicit PongStateManager(std::shared_ptr<GameState> currentState);

	virtual void handleEvents(const sf::Event& event) override;
	virtual void update(const float deltaTime) override;
	virtual void draw(sf::RenderWindow& winodw) override;

	virtual bool askingForNextState() override
	{
		return m_askingNextState;
	}

	virtual void askForNextState() override
	{
		m_askingNextState = true;
	}

	virtual const std::shared_ptr<GameState> getState() const override
	{
		return m_currentState;
	}

	virtual void setState(std::shared_ptr<GameState> state) override
	{
		m_currentState = state;
	}

	virtual const std::shared_ptr<GameState> getNextState() const override
	{
		return m_nextState;
	}

	virtual void setNextState(std::shared_ptr<GameState> nextState) override
	{
		m_nextState = nextState;
	}
private:
	std::shared_ptr<GameState> m_currentState;
	std::shared_ptr<GameState> m_nextState;

	bool m_askingNextState;
};

