#pragma once

#include <string>

#include <SFML/Graphics.hpp>

#include "PhysicalGameObject.h"

class Player : public PhysicalGameObject
{
public:
	enum class Type
	{
		PlayerOne,
		PlayerTwo
	};

	static const std::string Name;

	explicit Player(const Type& type, const float speed);

	virtual void handleEvents(const sf::Event& event) override;
	virtual void update(const float deltaTime) override;
	virtual void draw(sf::RenderWindow& window) override;

	virtual void onCollision(const std::shared_ptr<PhysicalGameObject>& other) override;

	virtual const sf::FloatRect getRect() const override
	{
		return m_sprite.getGlobalBounds();
	}
private:
	sf::Texture m_texture;
	sf::Sprite m_sprite;

	Type m_type;

	float m_speed;

	void loadSprite();
	void resetPosition();

	void updatePlayerOne(const float deltaTime);
	void updatePlayerTwo(const float deltaTime);
};