#include "PongStateManager.h"


PongStateManager::PongStateManager(std::shared_ptr<GameState> currentState)
	: m_currentState{ currentState },
	m_askingNextState{ false }
{

}

void PongStateManager::handleEvents(const sf::Event& event)
{
	m_currentState->handleEvents(event);
}

void PongStateManager::update(const float deltaTime)
{
	m_currentState->update(deltaTime);
}

void PongStateManager::draw(sf::RenderWindow& window)
{
	m_currentState->draw(window);
}