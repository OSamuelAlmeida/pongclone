#include "Player.h"

#include "PongGame.h"

const std::string Player::Name = "Player";

Player::Player(const Type& type, const float speed)
	: PhysicalGameObject{ {}, Name },
	m_type{ type }, 
	m_speed{ speed }
{
	loadSprite();
	resetPosition();
}

void Player::handleEvents(const sf::Event& event)
{

}

void Player::update(const float deltaTime)
{
	m_sprite.setPosition(m_position);

	switch (m_type)
	{
	case Type::PlayerOne:
		updatePlayerOne(deltaTime);
		break;
	case Type::PlayerTwo:
		updatePlayerTwo(deltaTime);
		break;
	}
}

void Player::draw(sf::RenderWindow& window)
{
	window.draw(m_sprite);
}

void Player::onCollision(const std::shared_ptr<PhysicalGameObject>& other)
{

}

void Player::loadSprite()
{
	m_texture.loadFromFile("assets/textures/bracket.png");
	m_sprite.setTexture(m_texture);
}

void Player::resetPosition()
{
	static const sf::Vector2f PlayerOnePosition = { 0.f, 0.f };
	static const sf::Vector2f PlayerTwoPosition = { static_cast<float>(PongGame::Constants::Window::Width - m_texture.getSize().x), 0.f };
	
	switch (m_type)
	{
	case Type::PlayerOne:
		m_position = PlayerOnePosition;
		break;
	case Type::PlayerTwo:
		m_position = PlayerTwoPosition;
		break;
	}
}

void Player::updatePlayerOne(const float deltaTime)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) && m_position.y >= 0)
	{
		m_position.y -= m_speed  * deltaTime;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && m_position.y <= PongGame::Constants::Window::Height - m_texture.getSize().y)
	{
		m_position.y += m_speed * deltaTime;
	}
}

void Player::updatePlayerTwo(const float deltaTime)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && m_position.y >= 0)
	{
		m_position.y -= m_speed  * deltaTime;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && m_position.y <= PongGame::Constants::Window::Height - m_texture.getSize().y)
	{
		m_position.y += m_speed * deltaTime;
	}
}