#pragma once

#include <memory>

#include "PhysicalGameObject.h"

namespace Physics
{
	extern bool intersects(const std::shared_ptr<PhysicalGameObject>& object1, const std::shared_ptr<PhysicalGameObject>& object2);
	extern bool collideWith(std::shared_ptr<PhysicalGameObject>& object, const std::vector<std::shared_ptr<PhysicalGameObject>>& others, std::shared_ptr<PhysicalGameObject>& otherObject);
}