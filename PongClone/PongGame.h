#pragma once

#include <memory>

#include <SFML/Graphics.hpp>

#include "Game.h"

#include "GameStateManager.h"

class PongGame : public Game
{
public:
	struct Constants
	{
		struct Window
		{
			static const std::string Title;
			static const unsigned int Width = 640;
			static const unsigned int Height = 480;
			static const unsigned int FramerateLimit = 60;
		};
	};

	explicit PongGame();

	virtual void run() override;
private:
	sf::RenderWindow m_window;

	sf::Clock m_clock;
	float m_deltaTime;

	std::shared_ptr<GameStateManager> m_gameStateManager;

	void handleEvents();
	void update();
	void draw();
};

