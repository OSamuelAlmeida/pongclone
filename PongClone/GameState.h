#pragma once

#include <SFML/Graphics.hpp>

class GameState
{
public:
	virtual ~GameState() { }

	virtual void handleEvents(const sf::Event& event) = 0;
	virtual void update(const float deltaTime) = 0;
	virtual void draw(sf::RenderWindow& window) = 0;
};