#include "PongGame.h"

#include "PongStateManager.h"
#include "MultiplayerState.h"

const std::string PongGame::Constants::Window::Title = "Pong Clone";

PongGame::PongGame()
	: m_window{ { Constants::Window::Width, Constants::Window::Height }, Constants::Window::Title },
	m_gameStateManager{ new PongStateManager{ std::make_shared<MultiplayerState>(m_gameStateManager) } }
{
	m_window.setFramerateLimit(Constants::Window::FramerateLimit);
}

void PongGame::run()
{
	while (m_window.isOpen())
	{
		m_deltaTime = m_clock.restart().asSeconds();

		handleEvents();
		update();
		draw();
	}
}

void PongGame::handleEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		m_gameStateManager->handleEvents(event);

		switch (event.type)
		{
		case sf::Event::Closed:
			m_window.close();
			break;
		}
	}
}

void PongGame::update()
{
	if (m_gameStateManager->askingForNextState() && m_gameStateManager->getNextState() != nullptr)
	{
		m_gameStateManager->setState(m_gameStateManager->getNextState());
	}

	m_gameStateManager->update(m_deltaTime);
}

void PongGame::draw()
{
	m_window.clear();

	m_gameStateManager->draw(m_window);

	m_window.display();
}