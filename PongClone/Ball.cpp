#include "Ball.h"

#include <ctime>

#include "PongGame.h"

#include "Player.h"

const std::string Ball::Name = "Ball";

Ball::Ball(const float speed)
	: PhysicalGameObject{ {}, Name },
	m_direction{ 1.f, 0.f },
	m_speed{ speed },
	m_collisionEnabled{ true },
	m_generator{ static_cast<unsigned int>(time(nullptr)) },
	m_randomAngle{ -1.f, 1.f }
{
	loadSprite();
	resetPosition();
}

void Ball::handleEvents(const sf::Event& event)
{

}

void Ball::update(const float deltaTime)
{
	updateCollisionTimer(deltaTime);
	updateScreenBounds();

	m_sprite.setPosition(m_position);

	m_position.x += m_speed * m_direction.x * deltaTime;
	m_position.y += m_speed * m_direction.y * deltaTime;
}

void Ball::draw(sf::RenderWindow& window)
{
	window.draw(m_sprite);
}

void Ball::onCollision(const std::shared_ptr<PhysicalGameObject>& other)
{
	if (m_collisionEnabled && other->getName() == Player::Name)
	{
		m_direction.x *= -1.f;
		m_direction.y *= -1.f;
		m_direction.y = m_randomAngle(m_generator);

		m_collisionEnabled = false;
		m_timer.restart();
	}
}

void Ball::loadSprite()
{
	m_texture.loadFromFile("assets/textures/ball.png");
	m_sprite.setTexture(m_texture);
}

void Ball::resetPosition()
{
	m_position = { static_cast<float>(PongGame::Constants::Window::Width / 2 - m_texture.getSize().x / 2), static_cast<float>(PongGame::Constants::Window::Height / 2 - m_texture.getSize().y / 2) };
	m_direction = { -m_direction.x, 0.f };
}

void Ball::updateCollisionTimer(const float deltaTime)
{
	if (!m_collisionEnabled && m_timer.getElapsedTime().asSeconds() > 1.f)
	{
		m_timer.restart();
		m_collisionEnabled = true;
	}
}

void Ball::updateScreenBounds()
{
	if (m_position.y <= 0 || m_position.y >= PongGame::Constants::Window::Height - m_texture.getSize().y)
	{
		m_direction.y *= -1.f;
	}
}