#pragma once

#include <SFML/Graphics.hpp>

#include "GameObject.h"

class DrawableGameObject : public GameObject
{
public:
	DrawableGameObject(const sf::Vector2f& position)
		: m_position{ position }
	{

	}

	virtual ~DrawableGameObject() { }

	virtual void draw(sf::RenderWindow& window) = 0;

	virtual const sf::Vector2f getPosition() const
	{
		return m_position;
	}
protected:
	sf::Vector2f m_position;
};