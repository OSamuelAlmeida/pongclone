#pragma once

#include <sstream>

template<typename T>
const std::string toText(const T value)
{
	std::ostringstream os;
	os << value;
	return os.str();
}