#pragma once

class Game
{
public:
	virtual ~Game() { }

	virtual void run() = 0;
};