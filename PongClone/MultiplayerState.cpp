#include "MultiplayerState.h"

#include "StringLibrary.h"

#include "PongGame.h"

#include "Physics.h"

#include "Player.h"

MultiplayerState::MultiplayerState(std::shared_ptr<GameStateManager>& gameStateManager)
	: m_gameStateManager{ gameStateManager },
	m_playerOneScore{ 0 },
	m_playerTwoScore{ 0 }
{
	createPhysicalGameObjects();
	setScoreTexts();
}

void MultiplayerState::handleEvents(const sf::Event& event)
{
	for (auto& physicalGameObject : m_physicalGameObjects)
	{
		physicalGameObject->handleEvents(event);
	}
}

void MultiplayerState::update(const float deltaTime)
{
	for (auto& physicalGameObject : m_physicalGameObjects)
	{
		physicalGameObject->update(deltaTime);
		updateCollisions(physicalGameObject);
		updateGameOver(physicalGameObject);
	}
}

void MultiplayerState::draw(sf::RenderWindow& window)
{
	for (auto& physicalGameObject : m_physicalGameObjects)
	{
		physicalGameObject->draw(window);
	}

	window.draw(m_playerOneScoreText);
	window.draw(m_playerTwoScoreText);
}

void MultiplayerState::updateCollisions(std::shared_ptr<PhysicalGameObject>& object)
{
	std::shared_ptr<PhysicalGameObject> other;
	
	if (Physics::collideWith(object, m_physicalGameObjects, other))
	{
		object->onCollision(other);
	}
}

void MultiplayerState::createPhysicalGameObjects()
{
	static const auto PlayerSpeed = 250.f;
	m_physicalGameObjects.push_back(std::make_shared<Player>(Player::Type::PlayerOne, PlayerSpeed));
	m_physicalGameObjects.push_back(std::make_shared<Player>(Player::Type::PlayerTwo, PlayerSpeed));

	static const auto BallSpeed = 250.f;
	m_ball = std::make_shared<Ball>(BallSpeed);
	m_physicalGameObjects.push_back(m_ball);
}

void MultiplayerState::setScoreTexts()
{
	m_font.loadFromFile("assets/fonts/ARCADECLASSIC.TTF");

	m_playerOneScoreText.setPosition({ 100.f, 50.f });
	m_playerTwoScoreText.setPosition({ 540.f, 50.f });

	m_playerOneScoreText.setFont(m_font);
	m_playerTwoScoreText.setFont(m_font);

	m_playerOneScoreText.setString("0");
	m_playerTwoScoreText.setString("0");

	m_playerOneScoreText.setColor(sf::Color::White);
	m_playerTwoScoreText.setColor(sf::Color::White);

	m_playerOneScoreText.setCharacterSize(24);
	m_playerTwoScoreText.setCharacterSize(24);
}

void MultiplayerState::updateGameOver(const std::shared_ptr<PhysicalGameObject>& currentObject)
{
	if (m_ball->getPosition().x < 0)
	{
		m_playerTwoScore += 1;
		m_ball->resetPosition();
		m_playerTwoScoreText.setString(toText(m_playerTwoScore));
	}
	else if (m_ball->getPosition().x > PongGame::Constants::Window::Width)
	{
		m_playerOneScore += 1;
		m_ball->resetPosition();
		m_playerOneScoreText.setString(toText(m_playerOneScore));
	}
}