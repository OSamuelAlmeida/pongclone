#pragma once

#include <memory>

#include <SFML/Graphics.hpp>

#include "GameState.h"

class GameStateManager
{
public:
	virtual ~GameStateManager() { }

	virtual void handleEvents(const sf::Event& event) = 0;
	virtual void update(const float deltaTime) = 0;
	virtual void draw(sf::RenderWindow& window) = 0;

	virtual bool askingForNextState() = 0;

	virtual void askForNextState() = 0;

	virtual const std::shared_ptr<GameState> getState() const = 0;
	virtual void setState(std::shared_ptr<GameState> state) = 0;

	virtual const std::shared_ptr<GameState> getNextState() const = 0;
	virtual void setNextState(std::shared_ptr<GameState> nextState) = 0;
};