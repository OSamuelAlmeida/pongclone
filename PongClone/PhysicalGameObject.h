#pragma once

#include <string>
#include <memory>

#include <SFML/Graphics.hpp>

#include "DrawableGameObject.h"

class PhysicalGameObject : public DrawableGameObject
{
public:
	PhysicalGameObject(const sf::Vector2f& position, const std::string& name)
		: DrawableGameObject{ position },
		m_name(name)
	{

	}

	virtual ~PhysicalGameObject() { }

	virtual void onCollision(const std::shared_ptr<PhysicalGameObject>& other) = 0;

	virtual const sf::FloatRect getRect() const = 0;

	virtual const std::string getName() const
	{
		return m_name;
	}
protected:
	std::string m_name;
};