#pragma once

#include <memory>
#include <vector>

#include <SFML/Graphics.hpp>

#include "GameState.h"
#include "GameStateManager.h"

#include "PhysicalGameObject.h"

#include "Ball.h"

class MultiplayerState : public GameState
{
public:
	explicit MultiplayerState(std::shared_ptr<GameStateManager>& gameStateManager);

	virtual void handleEvents(const sf::Event& event) override;
	virtual void update(const float deltaTime) override;
	virtual void draw(sf::RenderWindow& window) override;

	virtual void updateCollisions(std::shared_ptr<PhysicalGameObject>& object);
private:
	std::shared_ptr<GameStateManager>& m_gameStateManager;

	std::vector<std::shared_ptr<PhysicalGameObject>> m_physicalGameObjects;
	std::shared_ptr<Ball> m_ball;

	sf::Font m_font;
	sf::Text m_playerOneScoreText;
	sf::Text m_playerTwoScoreText;

	unsigned int m_playerOneScore;
	unsigned int m_playerTwoScore;

	void createPhysicalGameObjects();

	void setScoreTexts();

	void updateGameOver(const std::shared_ptr<PhysicalGameObject>& currentObject);
};