#pragma once

#include <SFML/Graphics.hpp>

class GameObject
{
public:
	virtual ~GameObject() { }

	virtual void handleEvents(const sf::Event& event) = 0;
	virtual void update(const float deltaTime) = 0;
};