#pragma once

#include <string>
#include <random>

#include <SFML/Graphics.hpp>

#include "PhysicalGameObject.h"

class Ball : public PhysicalGameObject
{
public:
	static const std::string Name;

	explicit Ball(const float speed);

	virtual void handleEvents(const sf::Event& event) override;
	virtual void update(const float deltaTime) override;
	virtual void draw(sf::RenderWindow& window) override;

	virtual void onCollision(const std::shared_ptr<PhysicalGameObject>& other) override;

	virtual const sf::FloatRect getRect() const override
	{
		return m_sprite.getGlobalBounds();
	}

	virtual const sf::Vector2f getDirection() const
	{
		return m_direction;
	}

	void resetPosition();
private:
	sf::Texture m_texture;
	sf::Sprite m_sprite;

	sf::Vector2f m_direction;
	float m_speed;

	sf::Clock m_timer;
	bool m_collisionEnabled;

	std::mt19937 m_generator;
	std::uniform_real_distribution<float> m_randomAngle;

	void loadSprite();

	void updateCollisionTimer(const float deltaTime);
	void updateScreenBounds();
};